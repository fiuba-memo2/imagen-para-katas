# Imagen para katas


## Utils

    docker build -t nicopaez/ruby:3.3.1 .
    docker run -it --rm -v$PWD:/workspace ruby:3.3.1 /bin/bash


## Version history

* 1.7.0: update a ruby 3.3.1
* 1.6.0: update a ruby 3.3.0
* 1.5.2: update a ruby 3.1.4
* 1.5.1: update de gemas
* 1.5.0: update a ruby 3.1.2
