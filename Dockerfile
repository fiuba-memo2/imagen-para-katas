FROM ruby:3.3.6
ENV VERSION=1.9.0
RUN mkdir setup
COPY  Gemfile /setup
COPY  Gemfile.lock /setup
WORKDIR /setup
RUN bundle install
WORKDIR /workspace
CMD ["/bin/sh"]